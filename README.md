
[]("如支持HTML标签解析，请使用这种格式。<p align='center'><a href='#'><img src='https://img.shields.io/badge/UninstallSoft-卸载恶意推广流氓软件-green?style=flat&logo=gitlab' width='100%'/></a></p>")

[![][Titel-主标题-图标]][Titel-主标题-链接]

---

[![][Menu-最新文档-图标]][Menu-最新文档-链接]
[![][Menu-许可协议-图标]][Menu-许可协议-链接]
[![][Menu-更新日志-图标]][Menu-更新日志-链接]
[![][Version-Release-图标]][Version-Release-链接]
[![][Common-收款码-图标]][Common-收款码-链接]

---

[![][Process-1-详情介绍-图标]][Process-1-详情介绍-链接]  

> &#9989; **为其他项目提供共用素材，提高空间复用率以及管理效率。**
>
> &#9989; **主要涉及一些项目的Demo演示。**  

[![][Process-2-特色说明-图标]][Process-2-特色说明-链接]  

> &#9989; **利用Bitbucket的产品特性做组合，提供存储一致性服务。**  

[![][Process-3-演示说明-图标]][Process-3-演示说明-链接]  

> &#9989; **仅作归档之用，无需演示说明。**  

[![][Process-4-使用说明-图标]][Process-4-使用说明-链接]  

> &#9989; **在其他项目中引用相关素材时，使用本库提供的素材链接。**  

[![][Process-5-授权许可-图标]][Process-5-授权许可-链接]  

> &#9989; **基于 Apache v2.0 协议授权许可**  

[![][Process-6-文件hash检验-图标]][Process-6-文件hash检验-链接]  

> &#9989; **归档文件相关hash检验请查看各具体相关归档文件。**  

[![][Process-7-赞赏与增值服务-图标]][Process-7-赞赏与增值服务-链接]  

> &#9989; [**如果你喜欢它，请点击我 ^_^ 并扫码赞赏支持一下吧 ：）**][Common-收款码-链接]  

> &#9989; **如果你需要增值服务（定制软件开发、互联网开发、物联网开发、数据库开发等），请与我联系哦：）**

![][Common-收款码-链接]

---

[]("无法访问，被墙了。 https://badgen.net/badge/Universal/共用素材/green?icon=atlassian&scale=5")

[Titel-主标题-图标]: https://img.shields.io/badge/Universal-共用素材-green?style=for-the-badge&logo=bitbucket
[Titel-主标题-链接]: #

[Menu-最新文档-图标]: https://img.shields.io/badge/Docs-Latest-green?style=flat&logo=microsoftword
[Menu-最新文档-链接]: README.md

[Menu-许可协议-图标]: https://img.shields.io/badge/license-Apache%20v2.0-blue?style=flat&logo=apache
[Menu-许可协议-链接]: LICENSE.md

[Menu-更新日志-图标]: https://img.shields.io/badge/changlogs-更新日志-white?style=flat&logo=appveyor
[Menu-更新日志-链接]: CHANGELOG.md

[Process-1-详情介绍-图标]: https://img.shields.io/badge/1-详情介绍-red?style=flat&logo=appveyor
[Process-1-详情介绍-链接]: #

[Process-2-特色说明-图标]: https://img.shields.io/badge/2-特色说明-orange?style=flat&logo=appveyor
[Process-2-特色说明-链接]: #

[Process-3-演示说明-图标]: https://img.shields.io/badge/3-演示说明-yellow?style=flat&logo=appveyor
[Process-3-演示说明-链接]: #

[Process-4-使用说明-图标]: https://img.shields.io/badge/4-使用说明-green?style=flat&logo=appveyor
[Process-4-使用说明-链接]: #

[Process-5-授权许可-图标]: https://img.shields.io/badge/5-授权许可-orange?style=flat&logo=appveyor
[Process-5-授权许可-链接]: #

[Process-6-文件hash检验-图标]: https://img.shields.io/badge/6-文件hash检验-blue?style=flat&logo=appveyor
[Process-6-文件hash检验-链接]: #

[Process-7-赞赏与增值服务-图标]: https://img.shields.io/badge/7-赞赏与增值服务-purple?style=flat&logo=appveyor
[Process-7-赞赏与增值服务-链接]: #

[Version-Release-图标]: https://img.shields.io/badge/release-v1.0.0.0001-green?style=flat&logo=bitbucket
[Version-Release-链接]: ../v1.0.0.0001

[Common-收款码-图标]: https://img.shields.io/badge/点击扫码-赞赏支持-red?style=flat&logo=paypal
[Common-收款码-链接]: Common/QRCode/Pay/Pay-QRCode.png
