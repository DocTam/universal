
[]("如支持HTML标签解析，请使用这种格式。<p align='center'><a href='#'><img src='https://img.shields.io/badge/UninstallSoft-卸载恶意推广流氓软件-green?style=flat&logo=gitlab' width='100%'/></a></p>")

[![][Titel-主标题-图标]][Titel-主标题-链接]

---

[![][Menu-最新文档-图标]][Menu-最新文档-链接]
[![][Menu-许可协议-图标]][Menu-许可协议-链接]
[![][Common-收款码-图标]][Common-收款码-链接]

---

[![][Version-Readme-图标]][Version-Readme-链接]

> &#128290; **主版本号.子版本号.修复版本号.编译版本号**
>
> &#9989; **主版本号：** 主要涉及重大（功能、资源）更新或改版或重建等；
>
> &#9989; **子版本号：** 主要涉及新增（功能、资源）或（功能、资源）调整以及优化等；
>
> &#9989; **修复版本号：** 主要涉及不影响（功能、资源）完整性时的BUG修复；
>
> &#9989; **编译版本号：** 程序编译自动变更，仅作标识用途；

---

[![][Version-1.0.0.0001-详情介绍-图标]][Version-1.0.0.0001-详情介绍-链接]

> &#9989; 1. **重构素材库；**
>
> &#9989; 2. **归类项目素材；**

---
[]("无法访问，被墙了。 https://badgen.net/badge/Universal/更新日志/green?icon=atlassian&scale=5")

[Titel-主标题-图标]: https://img.shields.io/badge/Universal-更新日志-green?style=for-the-badge&logo=bitbucket
[Titel-主标题-链接]: CHANGELOG.md

[Menu-最新文档-图标]: https://img.shields.io/badge/Docs-Latest-green?style=flat&logo=microsoftword
[Menu-最新文档-链接]: README.md

[Menu-许可协议-图标]: https://img.shields.io/badge/license-Apache2.0-blue?style=flat&logo=apache
[Menu-许可协议-链接]: LICENSE.md

[Menu-更新日志-图标]: https://img.shields.io/badge/changlogs-更新日志-white?style=flat&logo=appveyor
[Menu-更新日志-链接]: CHANGELOG.md

[Version-Readme-图标]: https://img.shields.io/badge/版本号-详细解读-white?style=flat&logo=appveyor
[Version-Readme-链接]: #

[Version-1.0.0.0001-详情介绍-图标]: https://img.shields.io/badge/2021.07.16-v1.0.0.0001-green?style=flat&logo=bitbucket
[Version-1.0.0.0001-详情介绍-链接]: ../v1.0.0.0001

[Common-收款码-图标]: https://img.shields.io/badge/点击扫码-赞赏支持-red?style=flat&logo=paypal
[Common-收款码-链接]: Common/QRCode/Pay/Pay-QRCode.png
